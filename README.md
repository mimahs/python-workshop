# **Description**

This repository contains the source code used for the ECESCON 12 workshop "Δίπλωμα Οδήγησης Δορυφόρου". 

## **Kinematic and Dynamic Models**
The Kinematic and Dynamic models shall be implemented in the `workshop_functions` file and are utilized for the needs of the `Detumbling_Simulation` and `Nadir_Pointing` simulation.

## **Detumbling Simulation** 
`Detumbling_Simulation` is a simulation script for the detumbling mode during which magnetic actuation is performed in order to dissipate the satellite's angular velocity (detumble). <br> For this simulation, participants should implement a Dissipative and a Bdot controller in the `workshop_functions` file.

## **Nadir Pointing  Simulation**
During nadir pointing mode both magnetorquers and reaction wheel are utilized to enable the reliable and continuous data transmission via the patch antenna to the Ground Station. <br> 
For this simulation, participants should implement a PD controller in the `workshop_functions` file. <br>
`Nadir_Pointing` is a simulation script for the nadir pointing mode and utilizes the Kinematic and Dynamic Models to acquire the satellite's orientation and angular velocity in each timestep. <br>
`Nadir_Pointing_Determination` is a simulation script for the nadir pointing mode and utilizes the Extended Kalman Filter (`EKF`) to acquire the satellite's orientation and angular velocity in each timestep.  
<br>
To overcome the time limitations, a cheatsheet file, which contains the implementation of all the controllers and models mentioned above, is available.

# **Useful links**
🌌️ [SpaceDot Website](https://spacedot.gr/) <br>
🛰️ [AcubeSAT Website](https://acubesat.spacedot.gr/) <br>
🤖️ [ADCS Website](https://acubesat.spacedot.gr/subsystems/attitude-determination-control/) <br>
📉️ [ADCS Simulations Repository](https://gitlab.com/acubesat/adcs/adcs-mekf) <br>
📚️ [DDJF AOCS](https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_AOCS.pdf) <br>
📚️ [AOCS Documentation](https://helit.org/mm/docList/public/ADC)
