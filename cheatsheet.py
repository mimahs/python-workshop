import numpy as np
from quaternions import *
from mtq_saturation import mtq_saturation


def PD_Controller(quaternion, angular_velocity):
    """
        Gets as input the quaternion representation of the satellite's orientation and the angular velocity and
        calculates the required torque to be applied so that the satellite performs nadir pointing 
    """
    q_desired = np.array([1, 0, 0, 0])
    Kp_gain = np.array([[0.0012, 0, 0], [0, 0.0012, 0], [0, 0, 0.0012]])
    Kd_gain = np.array([[0.001, 0, 0], [0, 0.001, 0], [0, 0, 0.001]])

    q_error = quatProd(quatConj(q_desired), quaternion)
    T_commanded = -np.matmul(Kp_gain, q_error[1:4]) - np.matmul(Kd_gain, angular_velocity)

    return T_commanded


def stateTransFun(state, torque, timestep):
    """
        Gets as input the quaternion representation of the satellite's orientation and the angular velocity and
        calculates the next orientation and angular velocity based on the kinematic and dynamic equations
    """

    inertia_matrix = np.array([[0.035, 0, 0], [0, 0.035, 0], [0, 0, 0.035]], dtype='d')

    quaternion = state[0:4]
    angular_velocity = state[4:7]
    angular_acceleration = np.matmul(np.linalg.inv(inertia_matrix),
                                     (torque + np.cross(np.matmul(inertia_matrix, angular_velocity), angular_velocity)))
    quaternion_next = quatProd(quaternion, quatExp(angular_velocity * timestep))
    angular_velocity_next = angular_velocity + angular_acceleration * timestep

    next_state = np.array([0 for i in range(7)], dtype='d')
    next_state[0:4] = np.array(quaternion_next)
    next_state[0:4] /= np.linalg.norm(next_state[0:4])
    next_state[4:7] = np.array(angular_velocity_next)

    return next_state


def Detumbling_Controller(Bdot, magnetic_field_body, ang_vel):
    """
        Gets as input the Bdot, the magnetic field expressed in the body frame and the angular velocity and calculates the required torque
        to be applied to achieve angular velocity dissipation. Satellite detumbling can be performed utilizing either a dissipative controller or a 
        Bdot controller. 
    """

    ## Bdot Controller
    # Kp = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    # magnetic_dipole = -np.matmul(Kp, Bdot)/np.linalg.norm(magnetic_field_body)
    # magnetic_dipole = mtq_saturation(magnetic_dipole)
    # torque = np.cross(magnetic_dipole, magnetic_field_body)

    ## Dissipative
    # Kp = np.array([[0.0001, 0, 0], [0, 0.0001, 0], [0, 0, 0.0001]])
    # torque = -np.matmul(Kp, ang_vel)

    return torque
