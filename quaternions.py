import math
import numpy as np


def quatExp(vRot):
    """
    Quaternion exponential map. This function calculates the quaternion (rotation) induced by the current angular velocity
    :param vRot - angular velocity
    :return quaternion
    """
    quaternion = np.array([1, 0, 0, 0], dtype='d')

    VRotNorm = np.linalg.norm(vRot)
    theta = VRotNorm

    if (VRotNorm > 1e-04):
        quaternion[0] = math.cos(theta / 2)
        quaternion[1:4] = (math.sin(theta / 2.0) * vRot) / (VRotNorm)


    return quaternion


def quatConj(quaternion):
    """
    Quaternion conjugate, meaning the vector part has the opposite sign. The returning quaternion represents the reversed rotation
    Meaning if the input has quaternion a 90 degree rotation along an axis, the conjugate has a 270=(360-90) degree rotation
    along the same axis.
    """
    conjugate = np.array([0, 0, 0, 0], dtype='d')
    conjugate[0] = quaternion[0]
    conjugate[1:4] = -quaternion[1:4]

    return conjugate


def quatProd(quat1, quat2):
    """
    Quaternion product return the quaternion (rotation) between two quaternions. Meaning that if the first quaternion rotates
    from a reference A to B and the second from B to C. The returning quaternion rotates from A to C.
    """
    temp1 = quat1[0] * quat2[0] - quat1[1] * quat2[1] - quat1[2] * quat2[2] - quat1[3] * quat2[3]
    temp2 = quat1[0] * quat2[1] + quat1[1] * quat2[0] + quat1[2] * quat2[3] - quat1[3] * quat2[2]
    temp3 = quat1[0] * quat2[2] - quat1[1] * quat2[3] + quat1[2] * quat2[0] + quat1[3] * quat2[1]
    temp4 = quat1[0] * quat2[3] + quat1[1] * quat2[2] - quat1[2] * quat2[1] + quat1[3] * quat2[0]
    quaternionProd = np.array([temp1, temp2, temp3, temp4], dtype='d')
    return quaternionProd


def vector_quaternion_turn(vector, quaternion):
    """
    Vector rotation based on a quaternion, aka the Hamilton Product
    """
    quaternion_vector = np.array([0, vector[0], vector[1], vector[2]], dtype='d')
    quat1 = quatProd(quaternion_vector, quaternion)
    quat2 = quatConj(quaternion)
    final_quaternion = quatProd(quat2, quat1)
    return final_quaternion[1:4]
